const model = require('../models'),
    {genSalt, hash, compareSync} = require('bcrypt')

const cryptPassword = async (password) => {
    const salt = await genSalt(12);

    return hash(password, salt)
}

module.exports = {
    register: (req, res) => {
        try {
            const data = await model.user.create({
                ...req.body,
                password: await cryptPassword(req.body.password)
            })

            return res.status(200).json({
                "success" : true,
                "error" : 0,
                "message" : "User successfully",
                "data" : data
            })
        }catch (error){
            return res.status(500).json({
                "success" : false,
                "error" : error.body,
                "message" : "User successfully",
                "data" : data
            })
        }
    }
}