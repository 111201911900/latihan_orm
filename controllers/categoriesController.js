const model = require('../models') // inisialisasi variabel model dengan yang berisi model di dalam folder models

module.exports = {
    list: async (req, res) => {
        try{
            const datas = await model.categories.findAll()

            return res.status(200).json({
                "success" : true,
                "error" : 0,
                "message" : "data succesfully listed",
                "data" : datas
            })
        }catch(error){
            return res.status(500).json({
                "success" : false,
                "error" : error.code,
                "message" : error,
                "data" : null
            })
        }
    },
    create: async(req,res) => {
        try{
            const data = await model.categories.create(req.body)

            return res.status(200).json({
                "success" : true,
                "error" : 0,
                "message" : "data succesfully created",
                "data" : data
            })
        }catch(error){
            return res.status(500).json({
                "success" : false,
                "error" : error.code,
                "message" : error,
                "data" : null
            })
        }
    },
    update: async(req,res) => {
        try{
            const data = await model.categories.update({
                name: req.body.name,
                description: req.body.description,
                is_active: req.body.is_active,
            },{
                where: {
                    id: req.body.id
                }
            })

            return res.status(200).json({
                "success" : true,
                "error" : 0,
                "message" : "data succesfully updated",
                "data" : data
            })
        }catch(error){
            return res.status(500).json({
                "success" : false,
                "error" : error.code,
                "message" : error,
                "data" : null
            })
        }
    },
    destroy: async(req,res) => {
        try{
            const data = await model.categories.destroy({
                where: {
                    id: req.body.id
                }
            })

            return res.status(200).json({
                "success" : true,
                "error" : 0,
                "message" : "data succesfully deleted",
                "data" : data
            })
        }catch(error){
            return res.status(500).json({
                "success" : false,
                "error" : error.code,
                "message" : error,
                "data" : null
            })
        }
    }
}
