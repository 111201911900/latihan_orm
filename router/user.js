const express = require('express') // inisiasi variable yang berisi express
const router = express.Router('') //inisiasi variable router yang berisi fungsi router dari express
const {register} = require('../controllers/userControllers.js')

router.post('/categories', categoriesRouter)

module.exports = router //export fungsi router agar dapat di baca pada modul lain