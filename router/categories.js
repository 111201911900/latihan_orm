const express = require('express') // inisiasi variable yang berisi express
const router = express.Router('') //inisiasi variable router yang berisi fungsi router dari express
const {list, create, update, destroy} = require('../controllers/categoriesController')
const validate = require('../middleware/validate')
const { createCategoriesRules } = require('../validators/rule')

router.get('./list', list) //router untuk enpoint list
router.post('./create', validate(createCategoriesRules), create)
router.put('./update', update)
router.delete('./destroy', destroy)

module.exports = router //export fungsi router agar dapat di baca pada modul lain