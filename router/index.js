const express = require('express') // inisiasi variable yang berisi express
const router = express.Router('') //inisiasi variable router yang berisi fungsi router dari express
const categoriesRouter = require('./categories')

router.get('/check-health', (req, res) => res.send("Application up"))
router.use('/categories', categoriesRouter)

module.exports = router //export fungsi router agar dapat di baca pada modul lain