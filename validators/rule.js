const { body } = require('express-validator')

const createCategoriesRules = [
    body('name').notEmpty().withMessage('name is required')
]

module.exports = {
    createCategoriesRules
}
