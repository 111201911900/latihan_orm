'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class categories extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.product)
    }
  }
  categories.init({
    name: {
      type:DataTypes.STRING,
      allowNull: false
    },
    description: {
      type:DataTypes.STRING,
      allowNull: true
    },
    is_active: {
      type:DataTypes.BOOLEAN,
      default: true
    }
  }, {
    sequelize,
    modelName: 'categories',
    tableName: 'categories',
    timestamps: true
  });
  return categories;
};